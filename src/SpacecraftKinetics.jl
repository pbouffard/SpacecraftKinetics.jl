module SpacecraftKinetics

using LinearAlgebra
using SpacecraftKinematics.Rotations
using GLMakie
using Test

export eom, integrate, u1, zeros3, ref_sinusoid, u2, cc1problem4, cc1problems45, test_integrate

function eom(;J, ωBN_N, σBN, u, L)
  CBN = MRP2DCM(σBN)
  CNB = CBN'
  Jinv = inv(J)
  ωBN_B = CBN * ωBN_N
  ω̇BN_B = Jinv * (-tilde(ωBN_B) * J * ωBN_B + u + L)
  Bmat = MRP_Bmatrix(σBN)
  # σ̇BN = 1/4 * ((1 - normsq(σBN))*I + 2*tilde(σBN) + 2*σBN*σBN')*ωBN_B
  σ̇BN = 1/4 * Bmat * ωBN_B
  ω̇BN_N = CNB * ω̇BN_B
  (; ω̇BN_N, σ̇BN)
end

zeros3(args...; kwargs...) = zeros(3)

function ref_sinusoid(freq, t)
  σRN = [0.2 * sin(freq*t); 0.3 * cos(freq*t); -0.3 * sin(freq*t)]
  σ̇RN = [0.2 * freq * cos(freq*t); -0.3 * freq * sin(freq*t); -0.3 * freq * cos(freq*t)]
  σ̈RN = [-0.2 * freq^2 * sin(freq*t); -0.3 * freq^2 * cos(freq*t); 0.3 * freq^2 * sin(freq*t)]
  ωRN_R = MRP_ω_from_σ_and_σ̇(σRN, σ̇RN)
  ω̇RN_R = MRP_ω̇_from_σ_and_derivs(σRN, σ̇RN, σ̈RN)
  CRN = MRP2DCM(σRN)
  CNR = CRN'
  ωRN_N = CNR * ωRN_R
  ω̇RN_N = CNR * ω̇RN_R
  (;σRN, σ̇RN, ωRN_N, ω̇RN_N)
end

ref_zero(t) = (;σRN=zeros(3,1), σ̇RN=zeros(3,1), ωRN_N=zeros(3,1), ω̇RN_N=zeros(3,1))

function integrate(;J, ωBN_N_t0, σBN_t0, u_fn=zeros3, tfinal, L_fn=zeros3, dt=0.001)
  ωBN_N, σBN = copy.([ωBN_N_t0, σBN_t0])
  tvec = LinRange(0:dt:tfinal)
  N = length(tvec)
  ωBN_N_vec = zeros((3, N))
  σBN_vec = zeros((3, N))
  # @show tvec
  u = zeros((3,1))
  for (i, t) in enumerate(tvec)
    # @show u
    L = L_fn(t)
    xdot = eom(; J, ωBN_N, σBN, u, L)
    u = u_fn(; t, ωBN_N, xdot.ω̇BN_N, σBN, xdot.σ̇BN)
    ωBN_N += xdot.ω̇BN_N * dt
    ωBN_N_vec[:,i] = ωBN_N
    # σBN = compose_mrp(σBN, xdot.σ̇BN * dt)
    σBN += xdot.σ̇BN * dt # Week 3 CC#1 problem 4 needs this for correct answer
    σBN = mrp_shadow(σBN)
    σBN_vec[:,i] = σBN
    # t += dt
    # @show normsq(σ)
    # @show t, ω, σ, xdot
  end
  (;tvec, σBN_vec, ωBN_N_vec)
end

function u(;t, J, K, P, σBN, σ̇BN, ωBN_N, σRN, ωRN_N, ω̇RN_N, L=zeros(3,1))
  σBR = compose_mrp(-σRN, σBN)
  CBN = MRP2DCM(σBN)
  ωRN_B = CBN * ωRN_N
  ωBR_N = ωBN_N - ωRN_N
  ωBR_B = CBN * ωBR_N
  ω̇RN_B = CBN * ω̇RN_N
  ωBN_B = CBN * ωBN_N
  -K*σBR - P*ωBR_B + J*(ω̇RN_B - tilde(ωBN_B) * ωRN_B) + tilde(ωBN_B)*J*ωBN_B - L
end



function motion_control(; J, ωBN_B_t0, σBN_t0, u_fn, ref_fn, tinfo, tfinal, L_fn=zeros3, dt=0.001)
  CBN_t0 = MRP2DCM(σBN_t0)
  ωBN_N_t0 = CBN_t0' * ωBN_B_t0
  
  result = integrate(; J, ωBN_N_t0, σBN_t0, u_fn, dt, tfinal, L_fn)
  # tinfo = problem_num == 4 ? 30 : 40
  step = 1
  irange = 1:step:length(result.tvec)
  t = result.tvec[irange]
  N = length(t)
  i_info = findfirst(>=(tinfo), t)
  # ref = problem_num == 5 ? ref_sinusoid.(0.05, t) : ref_zero.(t)
  ref = ref_fn.(t)
  # @show size(ref), size(t)
  σRN = hcat(getfield.(ref, :σRN)...)
  σBN = result.σBN_vec[:,irange]
  σBR = similar(σRN)
  for i=1:N
    σBR[:,i] = compose_mrp(-σRN[:,i], σBN[:,i])
  end
  σBR_norm = norm.(eachcol(σBR))
  σRN_info = σRN[:, i_info] #.σRN
  σBN_info = σBN[:, i_info]
  σBR_info = compose_mrp(-σRN_info, σBN_info)
  ωBN_N = result.ωBN_N_vec[:,irange]
  ωRN_N = hcat(getfield.(ref, :ωRN_N)...)
  # σBR_info =  -σRN_info + σBN_info
  # @show t[i_info], σRN_info, σBR_info, norm(σBR_info)
  result = merge(result, (;ref), (; t, σBN, σRN, ωBN_N, ωRN_N, σBR_norm, norm_σBR_info=norm(σBR_info)))
  result
end

function plot_results(; t, σBN, σRN, ωBN_N, ωRN_N, σBR_norm, unused_kwargs...)

  fig = Figure(resolution=(1600, 800))
  ax1 = fig[1,1] = Axis(fig, title="σ")
  colors = [:green, :blue, :red]

  pfunc1(y; kwargs...) = lines!(ax1, t, y; linewidth=3, kwargs...)
  
  line1 = pfunc1(σBN[1,:]; color=colors[1])
  # line1 = lines!(ax1, t, σBN[1,:], color=colors[1])
  line1r = pfunc1(σRN[1,:], color=colors[1], linestyle=:dash)
  line2 = pfunc1(σBN[2,:], color=colors[2])
  line2r = pfunc1(σRN[2,:], color=colors[2], linestyle=:dash)
  line3 = pfunc1(σBN[3,:], color=colors[3])
  line3r = pfunc1(σRN[3,:], color=colors[3], linestyle=:dash)
  line4 = pfunc1(σBR_norm.^2, color=:black)
  leg = fig[1,end+1] = Legend(fig, [line1, line2, line3, line4], [["σ$i" for i in 1:3]; "|σ|"])
  ylims!(ax1, [-1, 1.2])
  ax1.yticks = LinearTicks(15)

  # axnorm = fig[end+1,1] = Axis(fig, title="log₁₀|σBR|")
  # lines!(axnorm, t, log10.(σBR_norm))

  # ax2 = fig[end+1,1] = Axis(fig, title="ω")
  
  # pfunc2(y; kwargs...) = lines!(ax2, t, y; linewidth=3, kwargs...)
  # ω_line1 = pfunc2(ωBN_N[1,:], color=:red)
  # ω_line1r = pfunc2(ωRN_N[1,:], color=:purple)
  # ω_line2 = pfunc2(ωBN_N[2,:], color=:green)
  # ω_line2r = pfunc2(ωRN_N[2,:], color=:cyan)
  # ω_line3 = pfunc2(ωBN_N[3,:], color=:blue)
  # ω_line3r = pfunc2(ωRN_N[3,:], color=:magenta)
  display(fig)
  fig
end


function test_integrate()
  J = [100. 0 0; 0 75 0; 0 0 80]
  result = integrate(;J,  ωBN_N_t0=[0;0;1], σBN_t0=zeros(3,1), u_fn=zeros3, tfinal=120)
end

function wk3cc1problem4(;plot=true)
  ωBN_B_t0 = deg2rad.([30; 10; -20])
  σBN_t0 = [0.1; 0.2; -0.1]
  J = [100. 0 0; 0 75 0; 0 0 80]
  K = 5.0
  P = 10 * I(3)
  function u_fn(;t, σBN, σ̇BN, ωBN_N, unused_kwargs...)
    # ref = ref_sinusoid(0.05, t)
    u(;t, J, σBN, σ̇BN, ωBN_N, K, P, σRN=zeros(3,1), ωRN_N=zeros(3,1), ω̇RN_N=zeros(3,1))
  end
  tinfo = 30
  ref_fn = ref_zero #ref_sinusoid.(0.05, t)
  result = motion_control(; J, ωBN_B_t0, σBN_t0, u_fn, ref_fn, tinfo, tfinal=120)
  plot && plot_results(; result...)
  return result
end
export wk3cc1problem4

function wk3cc1problem5(;plot=true)
  ωBN_B_t0 = deg2rad.([30; 10; -20])
  σBN_t0 = [0.1; 0.2; -0.1]
  # ωBN_B_t0 = zeros(3,1) 
  # σBN_t0 = zeros(3,1) 
  J = [100. 0 0; 0 75 0; 0 0 80]
  K = 5.0
  P = 10 * I(3)
  dt = 0.01
  finite_differences = true
  function u_fn(;t, σBN, σ̇BN, ωBN_N, unused_kwargs...)
    ref = ref_sinusoid(0.05, t)
    ref_prev = ref_sinusoid(0.05, t - dt)
    ref_next = ref_sinusoid(0.05, t + dt)
    ω̇RN_N = finite_differences ? (ref_next.ωRN_N - ref_prev.ωRN_N)/(2 * dt) : ref.ω̇RN_N
    u(;t, J, σBN, σ̇BN, ωBN_N, K, P, σRN=ref.σRN, ωRN_N=ref.ωRN_N, ω̇RN_N=ω̇RN_N)
  end

  tinfo = 40
  ref_fn(t) = ref_sinusoid.(0.05, t)
  result = motion_control(; J, ωBN_B_t0, σBN_t0, u_fn, ref_fn, tinfo, tfinal=41, dt)
  fig = plot ? plot_results(; result...) : nothing
  return result, fig
end
export wk3cc1problem5

function wk3cc1tests()
  @testset "Week 3 Concept Check #1" begin
    @test isapprox(wk3cc1problem4(; plot=false).norm_σBR_info, 0.1949176787; atol=1e-4)
    @test isapprox(wk3cc1problem5(; plot=false).norm_σBR_info, 0.153; atol=1e-4)
    end
end
export wk3cc1tests

function wk3cc2problem5(;plot=true)
  ωBN_B_t0 = deg2rad.([30; 10; -20])
  σBN_t0 = [0.1; 0.2; -0.1]
  J = [100. 0 0; 0 75 0; 0 0 80]
  K = 5.0
  P = 10 * I(3)
  # function u_fn(;t, σBN, σ̇BN, ωBN_N, unused_kwargs...)
  #   ref = ref_sinusoid(0.05, t)
  #   u(;t, J=zeros(3,3), σBN, σ̇BN, ωBN_N, K, P, σRN=ref.σRN, ωRN_N=ref.ωRN_N, ω̇RN_N=ref.ω̇RN_N)
  # end

  dt = 0.001
  function u_fn(;t, σBN, σ̇BN, ωBN_N, unused_kwargs...)
    finite_differences = false
    ref = ref_sinusoid(0.05, t)
    ref_prev = ref_sinusoid(0.05, t - dt)
    ref_next = ref_sinusoid(0.05, t + dt)
    ω̇RN_N = finite_differences ? (ref_next.ωRN_N - ref_prev.ωRN_N)/(2 * dt) : ref.ω̇RN_N
    u(;t, J=zeros(3,3), σBN, σ̇BN, ωBN_N, K, P, σRN=ref.σRN, ωRN_N=ref.ωRN_N, ω̇RN_N=ω̇RN_N)
  end


  tinfo = 20
  ref_fn(t) = ref_sinusoid.(0.05, t)
  result = motion_control(; J, ωBN_B_t0, σBN_t0, u_fn, ref_fn, tinfo, tfinal=120)
  plot && plot_results(; result...)
  return result
end
export wk3cc2problem5

function wk3cc2problem6(;plot=true)
  ωBN_B_t0 = deg2rad.([30; 10; -20])
  σBN_t0 = [0.1; 0.2; -0.1]
  J = [100. 0 0; 0 75 0; 0 0 80]
  K = 5.0
  P = 10 * I(3)
  function u_fn(;t, σBN, σ̇BN, ωBN_N, unused_kwargs...)
    ref = ref_sinusoid(0.05, t)
    u(;t, J, σBN, σ̇BN, ωBN_N, K, P, σRN=ref.σRN, ωRN_N=ref.ωRN_N, ω̇RN_N=ref.ω̇RN_N)
  end

  tinfo = 80
  ref_fn(t) = ref_sinusoid.(0.05, t)
  L = [0.5;-0.3;0.2]
  result = motion_control(; J, ωBN_B_t0, σBN_t0, u_fn, ref_fn, L_fn=t->L, tinfo, tfinal=120)
  plot && plot_results(; result...)
end
export wk3cc2problem6

function wk3cc2problem7(;plot=true)
  ωBN_B_t0 = deg2rad.([30; 10; -20])
  σBN_t0 = [0.1; 0.2; -0.1]
  J = [100. 0 0; 0 75 0; 0 0 80]
  K = 5.0
  P = 10 * I(3)
  L = [0.5;-0.3;0.2]

  function u_fn(;t, σBN, σ̇BN, ωBN_N, unused_kwargs...)
    ref = ref_sinusoid(0.05, t)
    u(;t, J, σBN, σ̇BN, ωBN_N, K, P, σRN=ref.σRN, ωRN_N=ref.ωRN_N, ω̇RN_N=ref.ω̇RN_N, L)
  end

  tinfo = 80
  tfinal = 600
  ref_fn(t) = ref_sinusoid.(0.05, t)
  result = motion_control(; J, ωBN_B_t0, σBN_t0, u_fn, ref_fn, L_fn=t->L, tinfo, tfinal)
  plot && plot_results(; result...)
end
export wk3cc2problem7

function wk3cc2tests()
  @testset "Week 3 Concept Check #2" begin
    @test isapprox(wk3cc2problem5(; plot=false).norm_σBR_info, 0.1949176787; atol=1e-4)
  end
end
export wk3cc2tests

end # module
